class Division < ApplicationRecord
  belongs_to :title

  validates :title_id, presence: true, uniqueness: true
end
