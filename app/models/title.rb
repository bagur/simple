class Title < ApplicationRecord
  has_one :division

  validates :name, presence: true, length: {minimum: 2}, uniqueness: true
end
