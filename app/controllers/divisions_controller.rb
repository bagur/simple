class DivisionsController < ApplicationController
  def index
    @page = {
      nav_divisions: 'active'
    }
    @divisions = Title.joins(:divisions).all
  end

  def show
    @page = {
      nav_divisions: 'active'
    }
    @division = Division.find(params[:id])
  end

  def new
    @page = {
      nav_divisions: 'active'
    }
    @_content = 'Add new Division.';
    @division = Division.new
  end

  def create
    @page = {
      nav_divisions: 'active'
    }
    # render plain: params[:division].inspect
    @division = Division.new(division_params);
    if(@division.save)
      redirect_to @division
    else
      render 'new'
    end
  end

  private
  def division_params
    params.require(:division).permit(:title_id, :full_title, :tag)
  end
end
