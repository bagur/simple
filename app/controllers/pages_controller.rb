class PagesController < ApplicationController
  def about
    @page = {
      nav_about: 'active'
    }
    @_title = 'About Page';
    @_content = 'It is about us page.';
  end

  def home
    @page = {
      nav_home: 'active'
    }
    @_title = 'Home Page';
  end
end
