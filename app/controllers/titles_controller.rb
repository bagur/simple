class TitlesController < ApplicationController
  def index
    @page = {
      nav_titles: 'active'
    }
    @titles = Title.all
  end

  def show
    @page = {
      nav_titles: 'active'
    }
    @title = Title.find(params[:id])
  end

  def new
    @page = {
      nav_titles: 'active'
    }
    @_content = 'Add new Title.';
    @title = Title.new
  end

  def create
    @page = {
      nav_titles: 'active'
    }
    # render plain: params[:title].inspect
    @title = Title.new(title_params);
    if(@title.save)
      redirect_to @title
    else
      render 'new'
    end
  end

  def edit
    @page = {
      nav_titles: 'active'
    }
    @_content = 'Edit this Title.';
    @title = Title.find(params[:id])
  end

  def update
    @page = {
      nav_titles: 'active'
    }
    @title = Title.find(params[:id])
    if(@title.update(title_params))
      redirect_to @title
    else
      render 'edit'
    end
  end

  def destroy
    @page = {
      nav_titles: 'active'
    }
    @title = Title.find(params[:id])
    @title.destroy
    redirect_to titles_path
  end
  
  private
  def title_params
    params.require(:title).permit(:name, :note, :tags)
  end
end
