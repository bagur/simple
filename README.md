# README

Bootstrap 4 added.

So by now, Font Awesome 5 should be integrated into your project,
you can integrate the icon by inserting code like following:
```
<i class="fab fa-facebook-f fa-3x mx-2"></i>
```

One more thing ...
if you want to integrate Font Awesome into your rails erb code,
you might still need add following gem into your project.
```
# Gemfile

gem 'font_awesome5_rails'
```

So now you can start using ruby code like following to decorate your web page.
```
<%= fa_icon "baby", text: "BB", class: 'mx-2', size: '3x' %>
```
