Rails.application.routes.draw do
  get 'divisions/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "pages#home"

  get "home", to: "pages#home"
  get "about", to: "pages#about"
  
  # get "titles", to: "titles#index"
  resources :titles
  resources :divisions
  
end
