class CreateTitles < ActiveRecord::Migration[6.0]
  def change
    create_table :titles do |t|
      t.string :name, unique: true, null: false
      t.string :tags
      t.text :note

      t.timestamps
    end
  end
end
