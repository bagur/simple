class CreateDivisions < ActiveRecord::Migration[6.0]
  def change
    create_table :divisions do |t|
      t.belongs_to :title, index: { unique: true }, foreign_key: true
      t.string :full_title, index: { unique: true }
      t.string :tag

      t.timestamps
    end
  end
end
